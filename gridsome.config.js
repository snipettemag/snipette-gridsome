module.exports = {
	siteName: 'Snipette',
	siteUrl: 'https://www.snipettemag.com',
	siteDescription: 'Your weekly dose of perspective, understandibility and fun',
	titleTemplate: `%s - Snipette`,

	mediaUrlFrom: 'https://snipette-ghost.herokuapp.com/content/images/',
	mediaUrlTo: 'https://media.snipettemag.com/',

	templates: {
		GhostPost: '/:slug',
		GhostPage: '/:slug',
		GhostTag: '/tag/:slug',
		GhostAuthor: '/author/:slug'
	},

	plugins: [
		{
			use: '@gridsome/source-ghost',
			options: {
				baseUrl: process.env.GHOST_API_URL,
				contentKey: process.env.GHOST_CONTENT_KEY,
				routes: {
					post: '/:slug',
					page: '/:slug',
					author: '/author/:slug'
				}
			}
		},
		{
			use: '@gridsome/plugin-sitemap',
			options: {
				cacheTime: 600000, //default
				exclude: ['/not-ready/'],
			}
		},
		{
			use: '@microflash/gridsome-plugin-feed',
			options: {

				// (required) GraphQL collection types
				contentTypes: ['GhostPost'],

				// (optional) Properties used by feed API
				// See https://github.com/jpmonette/feed#example for all options
				feedOptions: {
					title: 'Snipette',
					description: 'Your weekly dose of understandibility, perspective and fun',
					link: 'https://www.snipettemag.com',
				},

				// (optional) Options for feed formats
				rss: {
					enabled: true,
					output: '/feed.xml',
				},
				atom: {
					enabled: true,
					output: '/feed.atom',
				},

				// (optional) an array of properties to be parsed as HTML
        // Converts relative URLs to absolute URLs
        // You can disable this by omitting the option
        htmlFields: ['html'],

        enforceTrailingSlashes: true,

        // (optional) sets the properties on each feed item
        // See https://github.com/jpmonette/feed#example for all options
        nodeToFeedItem: (node) => ({
					image: node.feature_image,
          title: node.title,
          date: new Date(node.published_at || null),
          content: '<img src="' + node.feature_image + '" style="height:auto;width:auto;" />' + node.html,
        }),
			},
		},
	]
}
