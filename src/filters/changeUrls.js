const config = require('../../gridsome.config.js');

export default string => {
  if (config.mediaUrlFrom && config.mediaUrlTo && string) {
    var processedString = string.replace(new RegExp(config.mediaUrlFrom, 'g'), config.mediaUrlTo);
  } else {
    var processedString = string;
  }
  return processedString;
}
