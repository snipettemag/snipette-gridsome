import '~/assets/css/style.scss'
import DefaultLayout from '~/layouts/Default.vue'
import moment from "vue-moment"
import InfiniteLoading from "vue-infinite-loading"

export default (Vue, {router, head, isClient}) => {
	Vue.component('Layout', DefaultLayout)
	Vue.use(moment)
	Vue.use(InfiniteLoading)
}
