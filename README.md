This is the source code for the upcoming [Snipette](https://beta.snipettemag.com)
website, build with [Ghost](https://ghost.org) and [Gridsome](https://gridsome.org/)
and using a customised version of Ghost's [casper](https://demo.ghost.io/)
theme.

This project is based on [Yashu Mittal](https://gitlab.com/mittalyashu)'s
**[Gridsome Starter Casper](https://gitlab.com/mittalyashu/gridsome-starter-casper)**
project and adapted to pull data via Ghost's API instead of through Markdown
files. We'll be updating it to suit our needs, but feel free to clone it
for your own use. (Just use your own posts instead of copying ours 😉)

## Deploy

To deploy the website, click the deploy button.

[![Deploy to Netlify](https://www.netlify.com/img/deploy/button.svg)](https://app.netlify.com/start/deploy?repository=https://gitlab.com/snipettemag/gridsome-ghost-site)

You will have to set the `GHOST_API_URL` and `GHOST_CONTENT_KEY` environment
variables. Alternatively, copy the contents of `sample-env` to a file
named `.env` (or `.env.development` or `.env.production`) and replace
the sample values with values from your own Ghost installation.

## Donate

This project relies heavily on the original **[Gridsome Starter Casper](https://gitlab.com/mittalyashu/gridsome-starter-casper)**
project. If you like it, you can support development of that project by
using the button below:

[![Become a Patron](https://i.imgur.com/wYOr44L.png)](https://www.patreon.com/bePatron?u=8494594)

If you like Snipette and want to support the magazine itself, please
check out our [Liberapay profile](https://liberapay.com/snipette) or
contact us through social media for more!
